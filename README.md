# Dockerization
A University project with automation in mind, FastAPI as it's core and with a bit of flavour given by Docker.

## Installation

Install dependencies with either [poetry](https://python-poetry.org/) or pip
```shell
poetry install
# or
pip install -r requirements-dev.txt
```

## Contributing
This repository uses [pre-commit](https://pre-commit.com/) for linting, formatting and keeping requirements files up-to-date.
Getting started:
```shell
pre-commit install
```

### Gitlab linting
Gitlab linting is done using a script located in the `scripts` folder and for it to works you must specify an environment variable called `GITLAB_TOKEN_FILE` which point to a file containing your gitlab access token.
This variable can be set from your shell using `export` or it can be specified in a file called `.env.pre-commit` like the following example
```text
GITLAB_TOKEN_FILE=your_token_filename
```

## Documentation
You can find the documentation [here](https://gitlab.com/api/v4/projects/41696100/jobs/artifacts/main/download?job=pdf-documentation)
