FROM python:3.10-slim as base

RUN apt update && apt upgrade -y && pip install --upgrade pip

# ----------------- BUILDER IMAGE
FROM base AS builder

ENV CONTAINER_USER="dockerization"

WORKDIR /home/${CONTAINER_USER}

RUN set -ex
RUN addgroup --system --gid 1000 $CONTAINER_USER && \
    adduser --system --uid 1000 --ingroup $CONTAINER_USER $CONTAINER_USER && \
    chown -R $CONTAINER_USER:$CONTAINER_USER /home/${CONTAINER_USER}

USER $CONTAINER_USER

COPY ./app ./app
COPY ./alembic.ini ./alembic.ini
COPY ./alembic ./alembic
COPY ./docker-entrypoint.sh .

USER root

EXPOSE 80

# ----------------- PRODUCTION BASE IMAGE
FROM base as production-base
COPY ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt --no-cache-dir

# ----------------- TESTING BASE IMAGE
FROM base as testing-base
COPY ./requirements-dev.txt ./requirements.txt
RUN pip install -r requirements.txt --no-cache-dir

# ----------------- PRODUCTION IMAGE
FROM builder as production

USER $CONTAINER_USER

COPY --from=production-base "/usr/local/lib/python3.10/site-packages" "/usr/local/lib/python3.10/site-packages"

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["python", "-m", "uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]

# ----------------- TESTING IMAGE
FROM builder as testing

USER $CONTAINER_USER

COPY --from=testing-base "/usr/local/lib/python3.10/site-packages" "/usr/local/lib/python3.10/site-packages"

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["python", "-m", "pytest"]
