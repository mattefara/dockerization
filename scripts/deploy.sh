#!/bin/sh

mkdir -p ~/.aws
echo -e "$AWS_DEPLOY_CREDENTIALS" > ~/.aws/credentials

apk update && apk upgrade && apk add git gettext

envsubst < templates/Dockerrun.aws.json > Dockerrun.aws.json

pip install awsebcli
eb init -p $AWS_PLATFORM -r $AWS_REGION $APP_NAME
eb use $APP_ENVIRONMENT
eb deploy
