#!/bin/sh

set -e

if [ "$1" = "python" ]; then
  python -m alembic upgrade head
fi

exec "$@"
