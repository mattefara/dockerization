\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{graphicx}
\usepackage{float}
\usepackage{biblatex}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{csquotes}

\addbibresource{./bibliography/bibliography.bib}
\graphicspath{./images/}
\lstset{
    breaklines=true
}

\title{Progetto Cloud and Edge Computing: Dockerization}
\author{Matteo Faraci}
\date{09/02/2023}

\begin{document}

\maketitle

\tableofcontents
\newpage

\section{Introduzione}
Dockerization è un progetto universitario volto alla scoperta e utilizzo di tecnologie che mirano all'automatizzazione del processo di sviluppo di un applicativo web fino al suo deployment.
In questo report verranno elencati tutti i passaggi fatti per raggiungere tale obiettivo, tentando di ottenere una buona base di partenza da cui continuare a sviluppare.
Tali risultati verranno ottenuti tramite l'utilizzo di scripts, librerie e pipeline CI/CD che, una completati, permetteranno agli sviluppatori di concentrarsi maggiormente sulla fase di sviluppo mentre il resto viene applicato automaticamente, aumentando la produttività.

\section{Tecnologie utilizzate}\label{sec:tecnologie}
Si è deciso di utilizzare queste tecnologie: FastAPI come base dell'applicativo web, Alembic per la gestione delle migrazioni al database, Docker per la costruzione dei container, Docker Compose come orchestratore ed AWS per la fase di deploy.

\subsection{FastAPI e Alembic}
FastAPI è un framework semplice ed intuitivo che permette di sviluppare applicazioni web velocemente assicurando la validazione dei dati, garantita dalla libreria pydantic.
Nel progetto FastAPI è stato utilizzato il minimo indispensabile: solo come base di partenza ed in modo molto marginale, in quanto l'obiettivo del progetto non riguarda lo sviluppo dell'applicazione in se.
Si compone di solo due endpoint: uno ritorna "Hello World" mentre l'altro se l'applicazione funziona correttamente.
Alembic invece è un'altra libreria dedicata alla gestione delle migrazioni del database, appoggiandosi a sqlalchemy.
Si è scelta questa libreria perchè molto intuitiva, anche se richiede una piccola fase di setup per essere utilizzata agevolmente insieme e FastAPI.

\subsection{Docker}
Docker è un software che come obiettivo ha la costruzione, condivisione e lancio di applicazioni.
Tutto viene gestito tramite containers e ognuno di questi opera in un ambiente isolato, così da evitare interferenze, anche tra due container derivati dalla immagine.
Con immagine si intende la struttura di un container e include tutte le operazioni che portano alla sua costruzione e lancio.
Docker permette la comunicazione dei container e uno strumento che mette a disposizione è docker-compose.
Questo strumento è in grado di gestire, o per meglio dire orchestrare, i containers rappresentati sotto forma di servizi.
I container possono essere anche condivisi tramite un registry, ovvero un luogo dove è possibile caricarli, così da poterli utilizzare su una macchina che non sia quella di sviluppo.

\subsection{AWS}
Amazon Web Services è una delle piattaforme cloud più utilizzate, capace di gestire migliaia di istanze delle propria applicazione.
Fornisce un'infinità di strumenti di monitoraggio con possibilità di scaling orizzontale potenzialmente illimitato.
In questo progetto si farà uso di Elastic Compute Cloud gestito tramite Elastic Beanstalk per il deploy dell'applicazione, così che possa essere reso disponibile pubblicamente.

\subsection{Gitlab}
Gitlab è una piattaforma online che si occupa di ospitare le repository git ed in questi anni sta fornendo sempre più funzionalità volte all'automatizzazione del processo di sviluppo.
Questo è il caso delle pipeline che si compongono di diversi stages e job.
Gli stages permettono di definire, implicitamente ed ad alto livello, l'ordine dei job.
I job sono i veri e propri passi delle pipeline ed è possibile definirne le dipendenze in modo da poter modellare scenari complessi.

\subsection{Scripts}
Anche gli scripts sono una parte fondamentale del progetto perchè sono responsabili della chiamata di altri strumenti, come ad esempio la formattazione del codice anche prima che avvenga un commit, così da ridurre al più possibile problemi identificabili durante lo sviluppo.
Approffittando del linguaggio Python si è scelto pre-commit.
Questa libreria permette, tramite apposito file di configurazione chiamato `.pre-commit-config.yml", di definire quali operazioni effettuare, per esempio, prima di un commit così che in caso di errore l'operazione in corso possa essere annullata.

\section{Problemi e soluzioni}
In questa fase si descrivono nei dettagli le varie soluzione applicate per il raggiungimento dell'obiettivo finale.

\subsection{Definizione del container}
Docker è stato utilizzato per la costruzione delle immagini dei container, due in particolare: produzione e test tramite le build multistage.
Questa tipo di soluzione permette:
\begin{enumerate}
	\item Definire una sorta di ereditarietà permettendo la costruzione di una nuova immagine da una precedente
	\item Copiare file da un container ad un altro, ad esempio il file risultante ottenuto dopo la compilazione del sorgente, così da ridurre la dimensione dei containers
\end{enumerate}
In questo progetto si provano a sfruttare queste caratteristiche ed il Dockerfile ottenuto si compone di diversi stadi: base, builder, production-base, testing-base, production e testing.
Queste fasi del processo di build sono state pensate tentando di minimizzare i comandi duplicati.
\begin{figure}[H]
    \includegraphics[width=0.95\textwidth]{images/multi-stage-diagram.png}
    \caption{Diagramma del processo di build}
    \label{fig:build-diagram}
\end{figure}
La Figura \ref{fig:build-diagram} rappresenta al meglio come le immagini vengono gestite e collegate logicamente.
Una volta finito il processo di build si ottengono due immagini: production e testing.
La differenza nelle immagini risiede nel numero di librerie con le quali vengono costruite; in testing sono presenti quelle volte al lancio dei test e alla formattazione del codice mentre in production no, così da avere un'immagine finale di produzione che sia più leggera velocizzando il deployment.
Analizzando la Figura \ref{fig:build-diagram} da sinistra, si nota l'immagine di partenza: python:3.10-slim.
E' stata scelta questa immagine perchè con alpine, è necessario installare librerie extra per l'utilizzo del driver di collegamento a postgres così da appesantire l'immagine finale, rendendola simile alla versione slim.
Per questo motivo si è quindi optato per la versione scelta, siccome non necessita di alcun passaggio extra per poterla utilizzare.
Ora si analizzerà in dettaglio il processo di build.
\begin{figure}[H]
    \lstinputlisting[firstline=1,lastline=3]{../Dockerfile}
    \caption{Definizione immagine base}
    \label{fig:dockerfile-base}
\end{figure}
Come indicato dalla Figura \ref{fig:dockerfile-base} Base indica solamente un alias per l'immagine di partenza con pip aggiornato all'ultima versione.

\begin{figure}[H]
    \lstinputlisting[firstline=6,lastline=25]{../Dockerfile}
    \caption{Definizione immagine builder}
    \label{fig:dockerfile-builder}
\end{figure}
Nella fase indicata dalla Figura \ref{fig:dockerfile-builder}, inizia la build.
Il primo passaggio riguarda: la definizione della working directory e l'aggiunta di un utente con bassi privilegi che si occuperà dell'esecuzione dell'applicazione.
Come ultimi comandi vengono copiati: sorgenti, file con le migrazioni del database (gestite tramite alembic) e l'entrypoint docker.
Nell'entrypoint si verifica che il comando passato inizi con la stringa `python" così da lanciare le migrazioni; infine viene eseguito il comando.
Questa scelta permette di lanciare agevolmente altri comandi (bash, sh ...) senza dover cambiare entrypoint (Figura \ref{fig:docker-entrypoint}).
\begin{figure}[H]
    \lstinputlisting[firstline=5,lastline=10]{../docker-entrypoint.sh}
    \caption{Docker entrypoint}
    \label{fig:docker-entrypoint}
\end{figure}
Infine viene esposta la porta 80 così da permettere la comunicazione con l'esterno e l'utente viene reimpostato a root per installare le librerie.
I file di requirements vengono tralasciati di proposito perchè il download delle librerie viene fatto successivamente, indicati dalla Figura \ref{fig:dockerfile-production-base} e dalla Figura \ref{fig:dockerfile-testing-base}
\begin{figure}[H]
    \lstinputlisting[firstline=29,lastline=30]{../Dockerfile}
    \caption{Definizione immagine production-base}
    \label{fig:dockerfile-production-base}
\end{figure}
\begin{figure}[H]
    \lstinputlisting[firstline=34,lastline=35]{../Dockerfile}
    \caption{Definizione immagine testing-base}
    \label{fig:dockerfile-testing-base}
\end{figure}
Nei due passaggi appena elencati vengono utilizzati rispettivamente i file requirements.txt e requirements-dev.txt, contenenti in uno le dipendenze necessarie e nell'altro quelle di test.
Siccome si tratta di due immagini differenti, i file, una volta copiati, verranno rinominati in requirements.txt e si procederà all'installazione.
Questa soluzione è resa possibile dall'utilizzo di poetry.
Poetry è un gestore e risolutore di dipendenze che permette la suddivisione dell'ambiente di sviluppo in gruppi così da tenere separate le dipendenze.
Infine durante la generazione dei requirements, si può decidere quali gruppi includere ed escludere.
\begin{figure}[H]
    \lstinputlisting[firstline=39,lastline=46]{../Dockerfile}
    \caption{Definizione immagine production}
    \label{fig:dockerfile-production}
\end{figure}
\begin{figure}[H]
    \lstinputlisting[firstline=49,lastline=56]{../Dockerfile}
    \caption{Definizione immagine testing}
    \label{fig:dockerfile-testing}
\end{figure}
Nei passaggi indicati dalla Figura \ref{fig:dockerfile-production} e dalla Figura \ref{fig:dockerfile-testing} vengono costruite le immagini finali.
In particolare, vengono copiate le librerie e l'applicazione viene lanciata con l'apposito comando.

\subsection{Orchestrazione dei servizi}
L'orchestratore scelto è Docker Compose e si occupa di eseguire i seguenti servizi: database tramite postgres, l'applicativo web e un proxy per la connessione tramite HTTPS.
La definizione dei servizi, descritta nel docker-compose.yml è molto semplice.
Le uniche due note riguardano: l'healthcheck del database e la configurazione del proxy.
La prima, come descritta in Figura \ref{fig:docker-compose-healthcheck}, viene utilizzata per determinare se il container è in salute, ovvero quando è pronto a ricevere connessioni.
Il controllo viene fatto tramite un utility messa a disposizione direttamente nel container e serve a testare se il database è pronto.
Il test viene fatto ogni 10 secondi, per 5 volte, aspettando una risposta per 5 secondi.
Se il comando ritorna un valore diverso da 0, allora viene considerato non in salute.
Il risultato del controllo viene poi utilizzato dal servizio web, che prima di partire necessita che il database sia in salute.
\begin{figure}[H]
    \lstinputlisting[firstline=28,lastline=32]{../docker-compose.yml}
    \caption{Database healthcheck}
    \label{fig:docker-compose-healthcheck}
\end{figure}
Per quanto riguarda il proxy invece, si utilizza un'immagine offerta da linuxserver.
Questo container, una volta lanciato, è configurato per generare i certificati necessari alla connessione HTTPS supportandone il rinnovo con Let's Encrypt.
Inoltre incorpora una serie di software volti alla protezione, come ad esempio fail2ban.
Il file si configurazione si compone di due parti principali: la definizione del server in ascolto sulla porta 80 e sulla porta 443.
Sulla porta 80 viene semplicemente redirezionato il traffico sulla porta 443, così da garantire una connessione HTTPS.
Per quanto riguarda la configurazione della porta 443 viene fatta una cosa simile: viene redirezionato il traffico all'applicazione web.
Una particolarità riguarda la definizione delle variabili, tramite la keyword `set", che compongono l'URL del container di destinazione.
Questa soluzione permette ad nginx di continuare la sua esecuzione perchè nel momento un cui il container si avvia, l'applicazione web potrebbe non essere pronta, quindi non raggiungibile, facendo terminale il proxy con un errore.
Infine vengono aggiunte due regole che impediscono la richiesta degli endpoint `docs" e `redoc", siccome forniscono documentazione che potrebbe descrivere funzionamenti interni e non esposti pubblicamente.
Tutto questo è descritto in Figura \ref{fig:proxy-config}.
\begin{figure}[H]
    \lstinputlisting[firstline=17,lastline=24]{../proxy/nginx/site-confs/default.conf}
    \caption{Definizione del proxy in ascolto sulla porta 443}
    \label{fig:proxy-config}
\end{figure}

\subsection{Scripts, formattazione e validità del codice}
Questa fase viene gestita in maniera automatica dalla libreria `pre-commit" configurata tramite il file `.pre-commit-config.yml".
Il file di configurazione contiene una serie di hooks e rappresentano tutti i test che l'applicazione deve superare.
La libreria permette di definire gli hooks appoggiandosi a repository locali e remoti (scaricati al primo lancio) in modo da non dover includere nelle dipendenze gli strumenti di verifica.
In questo caso vengono lanciati script remoti e locali.
Nel progetto ha i seguenti utilizzi:
\begin{itemize}
	\item Controlli di correttezza del file `.gitlab-ci"
	\item Controlli sulla correttezza delle dipendenze appoggiandosi a poetry, generando i file di requirements
	\item Formattare correttamente il codice sorgente seguendo le direttive Python oppure riordinare e gli import e rimuovere quelli inutilizzati
\end{itemize}

Un altro punto di vantaggio di questa libreria è che agisce solamente sugli ultimi file modificati e aggiunti a git, così da non dover formattare l'intera code base ogni volta.

Per quanto riguarda gli scripts, all'interno dell'apposita cartella è stato aggiunto uno script chiamato `gitlab-ci-cd-api-lint.py" che è responsabile del controllo del file `.gitlab-ci" e viene chiamato tramite `pre-commit".
Il funzionamento è molto semplice: interroga un endpoint messo a disposizione da gitlab che verifica la correttezza di `.gitlab-ci" e restituisce eventuali errori.
Per poterlo lanciare è necessario un token per potersi autenticare, specificato come file nelle variabili d'ambiente, generato dall'interfaccia web di gitlab.

\subsection{Deploy su AWS}
La fase di deploy viene fatta tramite AWS servendosi del servizio Elastic Beanstalk.
Il motivo principale per cui è stato scelto questo servizio è perchè fornisce dominio, utilizzato poi dal proxy per la generazione di certificati per la connessione HTTPS.
Inoltre è semplice semplice da automatizzare, grazie ad una CLI immediata.

Per utilizzare il servizio, è necessario creare un'applicazione dall'interfaccia browser di AWS.
Nel momento della creazione verrà aggiunto un ambiente di sviluppo ("environment") che racchiude un bucket S3 e un'istanza EC2.
Il bucket viene utilizzato per salvare il codice, mentre l'istanza per eseguirlo e viene gestito tutto automaticamente.
All'interno del bucket S3 vengono caricate i file necessari al lancio dell'applicazione: il Docker Compose e le credenziali di accesso per il pull delle immagini tramite docker.
Inoltre è necessario impostare le variabili d'ambiente da passare ai container, inserendole dal pannello di amministrazione.

Solitamente vengono caricati tutti i file, ma si è scelto di caricare solo il minimo indispensabile grazie ad un file denominato `.ebignore" che permette di ignorare alcuni file.
Si è quindi definita un'espressione regolare che ignori prima tutti i file e che poi aggiunga quelli desiderati.
I file essenziali sono: docker-compose e Dockerrun.aws.json.
Con il secondo file è di configurazione e si trova nella cartella templates e ispezionandone il contenuto si nota un riferimento ad auth.json.
Quel file sarà caricato sul bucket S3 e conterrà le credenziali di accesso al registry di gitlab così da scaricare le immagini.

\subsection{Stadi della pipeline}
In questo caso gli stages individuati sono: documentation, build, test, tag e deploy.
Le fasi dalla build al deploy riguardano l'applicativo web, mentre documentation riguarda la generazione di questo documento.
Prima di passare alla descrizione degli stadi è necessario definire, in relazione alla Figura \ref{fig:ci-job-definition}, il comportamento generale di alcuni job.
Questo viene fatto tramite ancore.
In questa fase si definisce del valido YML che viene associato ad un'ancora tramite il simbolo '\&' (come una sorta di variabile) per poi essere utilizzato successivamente.
In `job-base" viene definito come un job debba essere affinchè possa utilizzare l'applicazione all'interno del container.
Ne definisce quindi le variabili e servizi.
Viene fatta una cosa simile con 'docker-configuration'; in questo caso viene definito un job che fa uso del container registry con le fasi di login e logout.
\begin{figure}[H]
    \lstinputlisting[firstline=22,lastline=50]{../.gitlab-ci.yml}
    \caption{Definizione delle ancore}
    \label{fig:ci-job-definition}
\end{figure}

\subsubsection{Documentation}
\begin{figure}[H]
    \lstinputlisting[firstline=167,lastline=177]{../.gitlab-ci.yml}
    \caption{Definizione della fase di documentazione}
    \label{fig:ci-documentation}
\end{figure}
Questa fase delle pipeline permette la generazione di questa stessa documentazione in formato PDF come illustrato dalla Figura \ref{fig:ci-documentation}.
In questo job viene utilizzata un'immagine per la generazione della documentazione presa da islandoftex.
L'immagine è stata scelta in modo che fosse la più completa così da evitare problemi di compilazione da TeX a PDF per mancanza di librerie.
Questo job viene lanciato solamente nel momento in cui avvengono dei cambiamenti all'interno della directory docs, così da evitare la generazione di una documentazione che non è stata modificata.
Inoltre il job fa uso di un semplicissimo script bash che lancia il comando 'latexmk' con alcuni parametri che definiscono la cartella di destinazione della compilazione così da poter scaricare la documentazione successivamente tramite artefatto.
\subsubsection{Build}
\begin{figure}[H]
    \lstinputlisting[firstline=62,lastline=75]{../.gitlab-ci.yml}
    \caption{Definizione della fase di build}
    \label{fig:ci-build}
\end{figure}
In questo stage vengono individuati due jobs differenti chiamati: 'build-testing' e 'build-production' che sono responsabili delle costruzioni delle immagini di test e produzione con relativo push al container registry.
Entrambi utilizzano l'ancora 'docker-configuration' come indicato dalla Figura \ref{fig:ci-build} così da rendere la definizione del job più pulita, evitando ripetizioni e concentrandosi solamente sulla parte di script.
I job sono indipendenti e possono essere paralleli.
\subsubsection{Test}
\begin{figure}[H]
    \lstinputlisting[firstline=81,lastline=105]{../.gitlab-ci.yml}
    \caption{Definizione della fase di test}
    \label{fig:ci-test}
\end{figure}
\begin{figure}[H]
    \lstinputlisting[firstline=107,lastline=121]{../.gitlab-ci.yml}
    \caption{Definizione della fase di test}
    \label{fig:ci-test-2}
\end{figure}
Questa fase è quella che contiene i jobs dedicati al test delle immagini ed in particolare verifica: codice, immagine di test con coverage ed immagine di produzione.
Il primo job serve a verificare il codice e l'integrità del progetto.
Con verifica del codice si intendono gli stessi controlli effettuati da pre-commit, ma questa volta sull'intera codebase.

Tornando ai jobs delle pipeline, il primo job riguarda appunto l'utilizzo di 'pre-commit' per verificare che il codice inviato sia conforme allo standard definito.
Questo job richiede che gli stadi di build siano stati completati, così da evitare di analizzare codice incorretto.
In questo caso si è deciso di definire una variabile d'ambiente che permetta lo skip di alcuni hooks di pre-commit come ad esempio il linting del file '.gitlab-ci.yml'.
Si è pensato a questo skip perchè se il job sta eseguendo, allora significa che il file è sintatticamente corretto.
Si passa poi alla fase di test della logica dell'applicazione, per verificare che tutto sia corretto è stato definito il job 'test-testing-container'.
Questo job utilizza l'immagine di test, applica le migrazioni al database tramite alembic ed esegue i test tramite coverage.
La coverage rappresenta un valore che indica la percentuale delle righe di codice testate durante i test.
Questo stadio genera un artefatto che permetterà a gitlab di mostrare il badge relativo alla copertura dei test come mostrato in Figura \ref{fig:badges}.
Infine si passa al test relativo all'immagine di produzione.
Siccome non si dispone di strumenti di test, quello che viene fatto è controllare che l'applicazione si avvii, interrogando un endpoint che ritorna lo stato interno dell'applicazione.
Questo è reso possibile tramite l'immagine di curl e grazie alla possibilità di connettere i vari servizi alla stessa rete virtuale, così da poter definire un hostname.
Durante questa fase si utilizza come immagine principale una con all'interno l'eseguibile curl e come servizi tutte le parti dell'applicazione.
Si assegna un alias ad ogni servizio così da rendere quel nome l'hostname del servizio per essere poi riutilizzato.
Facendo quindi la richiesta tramite curl, si cerca la stringa 'ok' all'interno della risposta per superare il test.
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.50\textwidth]{./images/badges.png}
	\end{center}
	\caption{Gitlab badges}
	\label{fig:badges}
\end{figure}
\subsubsection{Deploy}
La fase di deploy si compone di due stadi: modifica del tag (Figura \ref{fig:ci-deploy-1}) ed effettivo deploy (Figura \ref{fig:ci-deploy-2}).
Durante la fase di modifica del tag, si prende il container generato e testato precedentemente e gli si cambia il tag a leatest.
Per quanto riguarda la vera fase di deploy, i passaggi necessari affinchè l'applicazione possa essere distribuita sono diversi e siccome non si dispone di un'istanza EC2 sempre attiva si è scelto di rendere il passaggio manuale, tramite la keyword 'when' durante la definizione del job.
Lo script deploy.sh permette il lancio dell'applicazione su AWS utilizzando Elastic Beanstalk.
Nello specifico: siccome non si dispone delle chiavi di autenticazione si genera il file .aws/credentials, si ottiene il pacchetto con il comando envsubst che riceve in input in file con segnati i nomi delle variabili d'ambiente e ne ritorna uno con i loro valori (così da nascondere le informazioni) ed infine viene fatto il deploy tramite la cli di Elastic Beanstalk.
Il risultato finale è visibile in Figura \ref{fig:deploy-aws}.
\begin{figure}[H]
    \lstinputlisting[firstline=126,lastline=136]{../.gitlab-ci.yml}
    \caption{Definizione della fase di deploy}
    \label{fig:ci-deploy-1}
\end{figure}
\begin{figure}[H]
    \lstinputlisting[firstline=140,lastline=148]{../.gitlab-ci.yml}
    \caption{Definizione della fase di deploy}
    \label{fig:ci-deploy-2}
\end{figure}
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.95\textwidth]{./images/deploy.png}
	\end{center}
	\caption{Deploy su AWS}
	\label{fig:deploy-aws}
\end{figure}

\section{Esperimenti}
In questa sezione viene descritto un altro esperimento fatto: setup e hosting di un gitlab runner privato, così da non saturare gli usage quota.
\subsection{Hosting di un runner di gitlab}
Gitlab permette di scaricare i runner di gitlab.
Un runner è il programma che di fatto lancia un job quando la pipeline esegue
E' possibile hostare il proprio runner così il tempo di utilizzo dello stesso non viene conteggiato negli usage quotas si gitlab.
Il setup è molto semplice e ben documentato; viene messo a disposizione o un container o un eseguibile ed in questo caso si è optato per la prima soluzione.
Per lanciare il container è necessario questo comando:
\begin{lstlisting}
docker run -d --name gitlab-runner --restart always \
		-v $CONFIG_PATH:/etc/gitlab-runner
		-v /var/run/docker.sock:/var/run/docker.sock \
		gitlab/gitlab-runner
\end{lstlisting}
Così facendo si lancia un container con due volumi: uno con i file di configurazione del runner e uno con il socket per poter utilizzare docker da dentro il container.
Il secondo passaggio riguarda la registrazione di un nuovo runner ed è possibile farlo da dentro il container tramite `gitlab-runner register" ed inserendo le informazioni richieste, ottenendo il token di registrazione dall'interfaccia web di gitlab nelle impostazioni del progetto alla voce "CI/CD > Runners'.
Tramite il comando "gitlab-runner verify' si ottiene un feedback immediato dal container.
Se i passaggi sono fatti correttamente si otterrà una pagina come quella indicata in Figura \ref{fig:specific-runner}.
E' importante ricordare di disattivare i runner condivisi, così da utilizzare solo quello appena aggiunto.
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.95\textwidth]{./images/specific-runner.png}
	\end{center}
	\caption{Runner specifico}
	\label{fig:specific-runner}
\end{figure}
Occorre un altro passaggio prima di terminare l'installazione, è necessario modificare il file di configurazione del runner in modo tale da permettere a docket di fare il pull delle immagini tramite HTTPS perchè altrimenti il processo fallisce.
Fortunatamente la soluzione è molto semplice e con una piccola aggiunta al file di configurazione del runner alla voce `volumes" la si modifica in questo modo:
\begin{lstlisting}
volumes = ["/cache", "/certs/client"]
\end{lstlisting}
Così facendo, docker riuscirà ad ottenere i certificati effettuando tutte operazioni tramite HTTPS.

\section{Miglioramenti}
Il progetto può essere considerato lontano dal suo termine, infatti sono possibili diversi miglioramenti:
\begin{itemize}
	\item Esclusione del codice sorgente con i tests dall'immagine di produzione perchè inutilizzati
	\item Verificare e nel caso ottimizzare e riorganizzare i jobs della pipeline per velocizzare il deploy
	\item Utilizzare un orchestratore differente nel momento un cui l'applicazione dovesse iniziare a scalare come Kubernetes
	\item Utilizzare Postgresql non tramite servizi ma tramite istanze di database dedicate offerte da AWS
\end{itemize}
Questi cambiamenti potrebbero ritenersi necessari nel momento in cui l'applicazione necessiti di essere replicata.
\section{Risultati}
Con il termine del progetto sono state gettate le basi per un'infrastruttura che permetta il deploy di un'applicazione basata su FastAPI tramite Docker e AWS.
Il progetto risulta essere una base dalla quale partire a sviluppare la vera a propria applicazione.
Si è stato in grado di gestire ed integrare diversi sistemi e tecnologie che hanno permesso di: costruire i container, testarli, verificare la formattazione del codice sorgente e la pubblicazione per gli utenti.
\end{document}
