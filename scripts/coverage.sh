#!/bin/sh

# Do migrations
python -m alembic upgrade head

# Run coverage
python -m coverage run -m pytest
python -m coverage report
python -m coverage xml
