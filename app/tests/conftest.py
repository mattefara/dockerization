import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app.database.session import SessionLocal
from app.main import app


@pytest.fixture(name="session")
def session():
    with SessionLocal() as session:
        yield session


@pytest.fixture(name="test_application")
def test_application(session: Session):
    with TestClient(app) as test_app:
        yield test_app
