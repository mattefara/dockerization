from pydantic import BaseConfig, BaseSettings, Field, PostgresDsn


class Settings(BaseSettings):
    postgres_connection: PostgresDsn = Field(env="POSTGRES_CONNECTION")

    class Config(BaseConfig):
        env_file = ".env"


def get_settings():
    return Settings()


settings = get_settings()
