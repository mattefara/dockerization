#!/bin/python3

import argparse
import json
import os
import sys
from urllib import request

import dotenv

# Loading env
dotenv.load_dotenv(".env.gitlab-ci")

DEFAULT_GITLAB_URL = "https://gitlab.com"
DEFAULT_GITLAB_FILE = ".gitlab-ci.yml"
DEFAULT_API_ROUTE = "api/v4/ci/lint"
DEFAULT_API_PROJECT_ROUTE = "api/v4/projects/{project}/ci/lint"

DEFAULT_GITLAB_TOKEN = os.getenv("GITLAB_TOKEN_FILE", None)
DEFAULT_GITLAB_PROJECT = os.getenv("GITLAB_PROJECT", None)


def post(url, headers, data):
    req = request.Request(url, data=json.dumps(data).encode("utf-8"), headers=headers)
    with request.urlopen(req) as response:
        if response.code != 200:
            raise Exception(
                f"Response with code: {response.status_code}. {response.json()}."
            )
        return json.loads(response.read().decode("utf-8"))


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--url", help="Set the url", default=DEFAULT_GITLAB_URL)
    parser.add_argument("--file", help="Set the file name", default=DEFAULT_GITLAB_FILE)
    parser.add_argument(
        "--project",
        help="Set the project for namespace",
        default=DEFAULT_GITLAB_PROJECT,
    )
    parser.add_argument(
        "--token",
        help="Set the the token to access the gitlab API",
        default=DEFAULT_GITLAB_TOKEN,
    )
    parser.add_argument(
        "--extra-args", help="Extra arguments to pass to the request", default={}
    )
    parser.add_argument("--merged", help="Show merged file", default=False)
    return parser.parse_args()


def request_lint(
    base_url, filename, project=None, token=None, merged=False, extra_args={}
):
    if not project:
        url = f"{base_url}/{DEFAULT_API_ROUTE}"
    else:
        url = f"{base_url}/{DEFAULT_API_PROJECT_ROUTE}".format(project=project)

    if merged:
        url += "?include_merged_yaml=true"

    headers = {"Content-Type": "application/json"}
    if token:
        headers.update({"PRIVATE-TOKEN": token})

    data = {"content": open(filename).read(), **extra_args}
    response = post(url, headers=headers, data=data)
    return response


def parse_lint_from_key_array(json_data, key, prepend_message=""):
    extracted = json_data.get(key, [])
    return "\n".join([f"{prepend_message}{ext}" for ext in extracted])


def main():
    args = get_args()
    if not args.token:
        raise Exception(
            "Either set GITLAB_TOKEN_FILE environment variable or specify --token flag"
        )
    with open(args.token, "r") as token:
        json_data = request_lint(
            args.url,
            args.file,
            args.project,
            token.read(),
            args.merged,
            args.extra_args,
        )

    if args.merged:
        print(json_data["merged_yaml"])

    errors = parse_lint_from_key_array(json_data, "errors", prepend_message="Error\t")
    if errors:
        print(errors)
    if errors:
        sys.exit(1)

    warnings = parse_lint_from_key_array(
        json_data, "warnings", prepend_message="Warning\t"
    )
    if warnings:
        print(warnings)
    if warnings:
        sys.exit(1)

    print("Linting ok!")


if __name__ == "__main__":
    main()
