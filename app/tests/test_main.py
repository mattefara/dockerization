from fastapi import status

from app.database.session import SessionLocal
from app.models.user import User


def test_root(test_application):
    response = test_application.get("/")
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {"message": "Hello World"}


def test_migration(session: SessionLocal):
    users = session.query(User).all()
    assert users == []
